package com.heapsteep.controller;

import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;

@RestController
public class SQSController {
	
	Logger logger= LoggerFactory.getLogger(SQSController.class);
	AmazonSQS sqs = AmazonSQSClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).build();
	String queueUrl="https://sqs.ap-south-1.amazonaws.com/561475806715/heapsteep-SQS-2";
    
	@GetMapping("/")
	public void readMessages() {
    	
    	AmazonSQS sqs = AmazonSQSClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).build();
    			      	
    	String queueUrl="https://sqs.ap-south-1.amazonaws.com/561475806715/heapsteep-SQS-2";
    	ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl)
    			.withVisibilityTimeout(10)
    			.withWaitTimeSeconds(20)
    			.withMaxNumberOfMessages(10); 
    	
    	List<Message> sqsMessages = sqs.receiveMessage(receiveMessageRequest).getMessages();
    	
    	for(Message m: sqsMessages) {
    	  	System.out.println("Message: " + m.toString());
    	  	//sqs.deleteMessage(queueUrl, m.getReceiptHandle());
    	}
    }
	
	/*@SqsListener("heapsteep-SQS-2")
    public void loadMessageFromSQS(String message)  {
        logger.info("message from SQS Queue: {}",message);        
    }*/
	
	@GetMapping("/send/{message}")  
    public void sendMessageToQueue(@PathVariable String message) {
		sqs.sendMessage(queueUrl, message);
    }
}
