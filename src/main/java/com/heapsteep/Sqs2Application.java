package com.heapsteep;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;

import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.heapsteep.controller.SQSController;

@SpringBootApplication
public class Sqs2Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Sqs2Application.class, args);
		//new SQSController().readMessages();
	}
}
